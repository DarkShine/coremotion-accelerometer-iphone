//
//  ViewController.swift
//  CoreMotion iPhone
//
//  Created by Henrique Valcanaia on 8/7/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    var motionManager: CMMotionManager?
    var motionQueue: NSOperationQueue?
    var shouldStop = true
    
    // Mark: ViewController Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureCoreMotion()
        self.listAvailableSensors()
    }
    
    // Mark: CoreMotion init and config
    func configureCoreMotion(){
        self.motionManager = CMMotionManager()
        self.motionManager?.accelerometerUpdateInterval = 0.1
        
        self.motionQueue = NSOperationQueue.mainQueue()
    }
    
    func listAvailableSensors(){
        var str = ""
        if (self.motionManager!.accelerometerAvailable){
            str += "Acce"
        }
        if(self.motionManager!.gyroAvailable){
            str += " Gyro"
        }
        if(self.motionManager!.magnetometerAvailable){
            str += " Magnt"
        }
        if(self.motionManager!.deviceMotionAvailable){
            str += " Devmot"
        }
        self.label.text = "Available sensors: \(str)"
    }
    
    @IBAction func startStopAccelerometerUpdates() {
        shouldStop = !shouldStop
        if (shouldStop){
            self.motionManager?.stopAccelerometerUpdates()
            println("------------------------------------------")
        }else{
            self.motionManager?.startAccelerometerUpdatesToQueue(self.motionQueue!, withHandler: { (data: CMAccelerometerData?, error: NSError?) -> Void in
                if(error == nil){
                    if let d = data{
                        let v = Vector(accelerometerData: d)
                        println(String(format: "%.8f,%.8f,%.8f", v.ax, v.ay, v.az))
                        //                    let acceleration = d!.acceleration
                        //                    println("\(acceleration.x),\(acceleration.y),\(acceleration.z)")
                    }
                }else{
                    print(error?.localizedDescription)
                }
            })
        }
    }
    
}


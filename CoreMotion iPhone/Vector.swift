//
//  Vector.swift
//  CoreMotion iPhone
//
//  Created by Henrique Valcanaia on 8/7/15.
//  Copyright (c) 2015 DarkShine. All rights reserved.
//

import UIKit
import CoreMotion

class Vector: NSObject, Printable {
    
    var ax:Double!
    var ay:Double!
    var az:Double!
    var gx:Double!
    var gy:Double!
    var gz:Double!

    struct Constants {
        // A G is a unit of gravitation force equal to that exerted by the earth's gravitational field (9.81 m/s2).
        static let Gravity = 9.81
    }
    
    init(accelerometerData:CMAccelerometerData){
        super.init()
        let acceleration = accelerometerData.acceleration
        self.ax = self.gToAcceleration(acceleration.x)
        self.ay = self.gToAcceleration(acceleration.y)
        self.az = self.gToAcceleration(acceleration.z)
        self.gx = acceleration.x
        self.gy = acceleration.y
        self.gz = acceleration.z
    }
    
    private func gToAcceleration(g:Double) -> Double{
        return g * Constants.Gravity
    }
    
    override var description : String {
        return "\(self.ax),\(self.ay),\(self.az)"
    }
    
}
